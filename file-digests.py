#!/bin/python

# file-digests - CLI and GUI tool to display several digests of a file.
#
# Copyright (C) 2022 Alexandre Gambier <agambier.dev@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import sys
import argparse
from libs.filedigests import FileDigests

# app version
appVersionStr = "0.10"

# parse command line
parser = argparse.ArgumentParser(prog='file-digests',
                                 description='Computes several digests of an input file.')
parser.add_argument('file',
                    nargs=1,
                    help='File to digest.')
parser.add_argument('--gui',
                    action='store_true',
                    help='Displays the digest in a window instead of stdout.')
parser.add_argument('--version',
                    action='version',
                    version='%(prog)s ' + appVersionStr)
args = parser.parse_args()

# init some variables
currentPath = os.getcwd()
appPath = os.path.dirname(os.path.realpath(__file__))
fileDigests = FileDigests(args.file[0])

# Only CLI or using GUI ?
if not args.gui:
    if not fileDigests.isReady():
        print("ERROR: Can't access file !")
        sys.exit(1)

    if not fileDigests.digest():
        print("ERROR: Failed to compute the digests !")
        sys.exit(2)

    print("MD5     : %s" % fileDigests.md5Digest())
    print("SHA-1   : %s" % fileDigests.sha1Digest())
    print("SHA-256 : %s" % fileDigests.sha256Digest())
    print("SHA-512 : %s" % fileDigests.sha512Digest())

else:
    import wx
    from gui.mainwindow import MainWindow

    # Change current path during GUI initialization
    os.chdir(appPath)

    # create wx main app
    app = wx.App(False)

    # Create main window
    mainWindow = MainWindow(None, fileDigests)
    mainWindow.Show(True)

    # restore path
    os.chdir(currentPath)

    # go
    app.MainLoop()
