# file-digests - CLI and GUI tool to display several digests of a file.
#
# Copyright (C) 2022 Alexandre Gambier <agambier.dev@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import wx
from gui.widgets import MainWindowBase


# inherit from the MainWindows created in wxFowmBuilder
class MainWindow(MainWindowBase):
    # constructor
    def __init__(self, parent, fileDigests):
        # properties
        self.m_fileDigests = fileDigests
        self.m_firstActivate = True

        # Timer used to generate an even onStart
        self.m_timer = wx.Timer()

        # initialize parent class
        MainWindowBase.__init__(self, parent)
        self.SetTitle(os.path.basename(fileDigests.fileName()))

    # msgBox
    def m_msgBox(self, text, title="file-digest", style=wx.ICON_EXCLAMATION | wx.OK):
        # display a dialog box
        dlg = wx.MessageDialog(None, text, title, style)
        return dlg.ShowModal()

    # close the window
    def m_onCloseClick(self, event=None):
        self.Close()

    # on window activation
    def m_onActivate(self, event=None):
        if not self.m_firstActivate:
            return

        # first activation
        self.m_firstActivate = False

        # generate onStart
        self.m_timer.Bind(wx.EVT_TIMER, self.m_onStart)
        self.m_timer.StartOnce(100)

    # when application starts
    def m_onStart(self, event=None):
        if not self.m_fileDigests.isReady():
            self.m_msgBox('Invalid file path.', "ERROR", wx.ICON_ERROR | wx.OK)
            self.m_onCloseClick()
            return

        # computes digest
        wx.SetCursor(wx.HOURGLASS_CURSOR)
        result = self.m_fileDigests.digest()
        wx.SetCursor(wx.STANDARD_CURSOR)
        if not result:
            self.m_msgBox('Failed to compute digests.', "ERROR", wx.ICON_ERROR | wx.OK)
            self.m_onCloseClick()
            return

        # display digests
        self.m_md5Value.SetValue(self.m_fileDigests.md5Digest())
        self.m_sha1Value.SetValue(self.m_fileDigests.sha1Digest())
        self.m_sha256Value.SetValue(self.m_fileDigests.sha256Digest())
        self.m_sha512Value.SetValue(self.m_fileDigests.sha512Digest())

    # copy digest value to the clipboard
    def m_onCopyClick(self, event):
        # which digest ?
        if event.EventObject == self.m_md5CopyBt:
            value = self.m_md5Value.GetValue()
        elif event.EventObject == self.m_sha1CopyBt:
            value = self.m_sha1Value.GetValue()
        elif event.EventObject == self.m_sha256CopyBt:
            value = self.m_sha256Value.GetValue()
        elif event.EventObject == self.m_sha512CopyBt:
            value = self.m_sha512Value.GetValue()
        else:
            return

        # if clipboard.Open():
        if not wx.TheClipboard.IsOpened():
            if wx.TheClipboard.Open():
                wx.TheClipboard.SetData(wx.TextDataObject(text=value))
                wx.TheClipboard.Close()
            else:
                self.m_msgBox('Failed to open clipboard.', "ERROR", wx.ICON_ERROR | wx.OK)
        else:
            self.m_msgBox('Clipboard is blocked.', "ERROR", wx.ICON_ERROR | wx.OK)
