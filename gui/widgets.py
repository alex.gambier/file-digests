# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainWindowBase
###########################################################################

class MainWindowBase ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"file-digest", pos = wx.DefaultPosition, size = wx.Size( 600,230 ), style = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.SYSTEM_MENU|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		m_mainSizer = wx.BoxSizer( wx.VERTICAL )

		m_md5Sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_md5Label = wx.StaticText( self, wx.ID_ANY, u"MD5", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_md5Label.Wrap( -1 )

		m_md5Sizer.Add( self.m_md5Label, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_md5Value = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		m_md5Sizer.Add( self.m_md5Value, 5, wx.ALL, 5 )

		self.m_md5CopyBt = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 32,32 ), wx.BU_AUTODRAW|0 )

		self.m_md5CopyBt.SetBitmap( wx.Bitmap( u"gui/bmp/png/edit-copy.png", wx.BITMAP_TYPE_ANY ) )
		m_md5Sizer.Add( self.m_md5CopyBt, 0, wx.ALL, 2 )


		m_mainSizer.Add( m_md5Sizer, 0, wx.EXPAND, 5 )

		m_sha1Sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_sha1Label = wx.StaticText( self, wx.ID_ANY, u"SHA-1", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_sha1Label.Wrap( -1 )

		m_sha1Sizer.Add( self.m_sha1Label, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_sha1Value = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		m_sha1Sizer.Add( self.m_sha1Value, 5, wx.ALL, 5 )

		self.m_sha1CopyBt = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 32,32 ), wx.BU_AUTODRAW|0 )

		self.m_sha1CopyBt.SetBitmap( wx.Bitmap( u"gui/bmp/png/edit-copy.png", wx.BITMAP_TYPE_ANY ) )
		m_sha1Sizer.Add( self.m_sha1CopyBt, 0, wx.ALL, 2 )


		m_mainSizer.Add( m_sha1Sizer, 0, wx.EXPAND, 5 )

		m_sha256Sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_sha256Label = wx.StaticText( self, wx.ID_ANY, u"SHA-256", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_sha256Label.Wrap( -1 )

		m_sha256Sizer.Add( self.m_sha256Label, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_sha256Value = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		m_sha256Sizer.Add( self.m_sha256Value, 5, wx.ALL, 5 )

		self.m_sha256CopyBt = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 32,32 ), wx.BU_AUTODRAW|0 )

		self.m_sha256CopyBt.SetBitmap( wx.Bitmap( u"gui/bmp/png/edit-copy.png", wx.BITMAP_TYPE_ANY ) )
		m_sha256Sizer.Add( self.m_sha256CopyBt, 0, wx.ALL, 2 )


		m_mainSizer.Add( m_sha256Sizer, 0, wx.EXPAND, 5 )

		m_sha512Sizer = wx.BoxSizer( wx.HORIZONTAL )

		self.m_sha512Label = wx.StaticText( self, wx.ID_ANY, u"SHA-512", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_sha512Label.Wrap( -1 )

		m_sha512Sizer.Add( self.m_sha512Label, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_sha512Value = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		m_sha512Sizer.Add( self.m_sha512Value, 5, wx.ALL, 5 )

		self.m_sha512CopyBt = wx.BitmapButton( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 32,32 ), wx.BU_AUTODRAW|0 )

		self.m_sha512CopyBt.SetBitmap( wx.Bitmap( u"gui/bmp/png/edit-copy.png", wx.BITMAP_TYPE_ANY ) )
		m_sha512Sizer.Add( self.m_sha512CopyBt, 0, wx.ALL, 2 )


		m_mainSizer.Add( m_sha512Sizer, 0, wx.EXPAND, 5 )

		self.m_closeBt = wx.Button( self, wx.ID_ANY, u"Close", wx.DefaultPosition, wx.Size( -1,32 ), 0 )

		self.m_closeBt.SetBitmapPosition( wx.LEFT )
		m_mainSizer.Add( self.m_closeBt, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )


		self.SetSizer( m_mainSizer )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_ACTIVATE, self.m_onActivate )
		self.m_md5CopyBt.Bind( wx.EVT_BUTTON, self.m_onCopyClick )
		self.m_sha1CopyBt.Bind( wx.EVT_BUTTON, self.m_onCopyClick )
		self.m_sha256CopyBt.Bind( wx.EVT_BUTTON, self.m_onCopyClick )
		self.m_sha512CopyBt.Bind( wx.EVT_BUTTON, self.m_onCopyClick )
		self.m_closeBt.Bind( wx.EVT_BUTTON, self.m_onCloseClick )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def m_onActivate( self, event ):
		event.Skip()

	def m_onCopyClick( self, event ):
		event.Skip()




	def m_onCloseClick( self, event ):
		event.Skip()


