# file-digests - CLI and GUI tool to display several digests of a file.
#
# Copyright (C) 2022 Alexandre Gambier <agambier.dev@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import hashlib


class FileDigests:
    # constructor
    def __init__(self, fileName, md5=True, sha1=True, sha256=True, sha512=True):
        self.m_fileName = fileName
        self.m_md5 = md5
        self.m_sha1 = sha1
        self.m_sha256 = sha256
        self.m_sha512 = sha512
        self.reset()

    # resets digest values
    def reset(self):
        self.m_md5Digest = ""
        self.m_sha1Digest = ""
        self.m_sha256Digest = ""
        self.m_sha512Digest = ""

    # return true if ready to compute digests
    def isReady(self):
        return os.path.isfile(self.m_fileName)

    # returns file name
    def fileName(self):
        return self.m_fileName

    # returns md5 digest
    def md5Digest(self):
        return self.m_md5Digest

    # returns sha1 digest
    def sha1Digest(self):
        return self.m_sha1Digest

    # returns sha256 digest
    def sha256Digest(self):
        return self.m_sha256Digest

    # returns sha512 digest
    def sha512Digest(self):
        return self.m_sha512Digest

    # digest the file
    def digest(self):
        self.reset()

        # Open the file
        try:
            file = open(self.m_fileName, "rb")
        except Exception:
            return False

        # MD5
        if self.m_md5:
            file.seek(0)
            self.m_md5Digest = hashlib.md5(file.read()).hexdigest()

        # SHA1
        if self.m_sha1:
            file.seek(0)
            self.m_sha1Digest = hashlib.sha1(file.read()).hexdigest()

        # SHA256
        if self.m_sha256:
            file.seek(0)
            self.m_sha256Digest = hashlib.sha256(file.read()).hexdigest()

        # SHA512
        if self.m_sha512:
            file.seek(0)
            self.m_sha512Digest = hashlib.sha512(file.read()).hexdigest()

        file.close()
        return True
